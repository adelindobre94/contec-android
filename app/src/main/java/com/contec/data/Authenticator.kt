package com.contec.data

import android.content.Context

class Authenticator(val context: Context) {

    private val pref = context.getSharedPreferences("CONTEC", Context.MODE_PRIVATE)

    fun isLoggedIn(): Boolean {
        return pref.getBoolean(KEY_LOGIN, false)
    }

    fun login(
        email: String?,
        password: String?
    ): Boolean {
        if (email == "user@contec" && password == "1234") {
            pref.edit()
                .putBoolean(KEY_LOGIN, true)
                .apply()
            return true
        }
        return false
    }

    companion object {
        private const val KEY_LOGIN = "key-login"
    }
}