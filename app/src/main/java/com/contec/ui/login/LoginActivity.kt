package com.contec.ui.login

import android.animation.AnimatorSet
import android.animation.ObjectAnimator
import android.animation.PropertyValuesHolder
import android.animation.ValueAnimator
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.view.animation.AccelerateDecelerateInterpolator
import androidx.appcompat.app.AppCompatActivity
import androidx.interpolator.view.animation.FastOutSlowInInterpolator
import com.contec.app.R
import com.contec.data.Authenticator
import com.contec.ui.MainActivity
import kotlinx.android.synthetic.main.activity_login.*


class LoginActivity : AppCompatActivity() {

    private lateinit var authenticator: Authenticator

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        authenticator = Authenticator(applicationContext)
        if (authenticator.isLoggedIn()) {
            navigateToMain()
        }

        if (savedInstanceState == null)
            runEnterAnimation()

        setUpInputField()
    }

    private fun navigateToMain() {
        MainActivity.start(this)
        finish()
    }

    private fun setUpInputField() {
        input_email.isEndIconVisible = false
        et_email.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(editable: Editable?) {
                input_email.isEndIconVisible = editable?.toString()?.trim() == "user@contec"
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }
        })
        et_email.setText("user@contec")

        button_login.setOnClickListener { v -> login() }
    }

    private fun login() {
        val email = et_email.text?.toString()
            ?.trim()
        val password = et_pwd.text?.toString()
            ?.trim()
        if (authenticator.login(email, password)) {
            navigateToMain()
        }
    }

    private fun runEnterAnimation() {
        input_email.alpha = 0f
        input_email.scaleX = 1.05f
        input_pwd.alpha = 0f
        input_pwd.scaleX = 1.05f
        button_login.alpha = 0f
        button_forgot_pwd.alpha = 0f

        cv_login.alpha = 0f
        tv_enroll.alpha = 0f
        button_new_account.alpha = 0f

        findViewById<View>(android.R.id.content).post {
            val startY = resources.displayMetrics.heightPixels / 2 - imgLogo.height / 2f
            val endY = imgLogo.y

            imgLogo.y = startY
            input_email.translationY += input_email.height
            input_pwd.translationY += input_pwd.height
            button_login.translationY += button_login.height

            val logoAnimator = ObjectAnimator.ofFloat(imgLogo, View.Y, startY, endY)
            logoAnimator.duration = DURATION
            logoAnimator.startDelay = 300

            val cardviewAnimator = ObjectAnimator.ofPropertyValuesHolder(
                cv_login,
                PropertyValuesHolder.ofFloat(View.TRANSLATION_Y, 0f),
                PropertyValuesHolder.ofFloat(View.ALPHA, 1f),
                PropertyValuesHolder.ofFloat(View.SCALE_X, 1f)
            )
            cardviewAnimator.startDelay = 450
            cardviewAnimator.duration = 250

            val emailInputAnimator = ObjectAnimator.ofPropertyValuesHolder(
                input_email,
                PropertyValuesHolder.ofFloat(View.TRANSLATION_Y, 0f),
                PropertyValuesHolder.ofFloat(View.ALPHA, 1f),
                PropertyValuesHolder.ofFloat(View.SCALE_X, 1f)
            )
            emailInputAnimator.startDelay = 450
            emailInputAnimator.duration = 250

            val pwdInputAnimator = ObjectAnimator.ofPropertyValuesHolder(
                input_pwd,
                PropertyValuesHolder.ofFloat(View.TRANSLATION_Y, 0f),
                PropertyValuesHolder.ofFloat(View.ALPHA, 1f),
                PropertyValuesHolder.ofFloat(View.SCALE_X, 1f)
            )
            pwdInputAnimator.startDelay = 450
            pwdInputAnimator.duration = 250

            val btnForgotpwdAnimator = ObjectAnimator.ofPropertyValuesHolder(
                button_forgot_pwd,
                PropertyValuesHolder.ofFloat(View.TRANSLATION_Y, 0f),
                PropertyValuesHolder.ofFloat(View.ALPHA, 1f),
                PropertyValuesHolder.ofFloat(View.SCALE_X, 1f)
            )
            btnForgotpwdAnimator.startDelay = 450
            btnForgotpwdAnimator.duration = 250

            val btnLoginAnimator = ObjectAnimator.ofPropertyValuesHolder(
                button_login,
                PropertyValuesHolder.ofFloat(View.TRANSLATION_Y, 0f),
                PropertyValuesHolder.ofFloat(View.ALPHA, 1f)
            )
            btnLoginAnimator.startDelay = 500
            btnLoginAnimator.duration = 200

            val tvEnrollAnimator = ObjectAnimator.ofPropertyValuesHolder(
                tv_enroll,
                PropertyValuesHolder.ofFloat(View.TRANSLATION_Y, 0f),
                PropertyValuesHolder.ofFloat(View.ALPHA, 1f)
            )
            tvEnrollAnimator.startDelay = 500
            tvEnrollAnimator.duration = 200

            val btnNewAccAnimator = ObjectAnimator.ofPropertyValuesHolder(
                button_new_account,
                PropertyValuesHolder.ofFloat(View.TRANSLATION_Y, 0f),
                PropertyValuesHolder.ofFloat(View.ALPHA, 1f)
            )
            btnNewAccAnimator.startDelay = 500
            btnNewAccAnimator.duration = 200

            val set = AnimatorSet()
            set.interpolator = FastOutSlowInInterpolator()
            set.playTogether(
                logoAnimator, cardviewAnimator, emailInputAnimator, pwdInputAnimator, btnForgotpwdAnimator,
                btnLoginAnimator, tvEnrollAnimator, btnNewAccAnimator)
            set.start()
        }
    }

    companion object {
        private const val DURATION = 500L
    }
}
